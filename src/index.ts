import "reflect-metadata";
import express from "express";
import cors from "cors";
import path from "path";
import { ApolloServer } from "@apollo/server";
import { expressMiddleware } from "@apollo/server/express4";
import { buildSchema } from "type-graphql";
import { resolvers } from "@generated/type-graphql";
import { PrismaClient } from "@prisma/client";
import { Context } from "./context";

const prisma = new PrismaClient();

const app = express();

app.use(express.json());
app.use(cors<cors.CorsRequest>({
  origin: '*', //"http://localhost:5173", TODO replace
  credentials: true,
}));

const main = async () => {

  const schema = await buildSchema({
    resolvers,
    validate: false,
    emitSchemaFile: path.resolve(__dirname, "graphql", "schema.graphql"),
  });
  const apollo = new ApolloServer<Context>({
    schema,
  });

  await apollo.start();

  app.use(
    "/graphql",
    express.json(),
    expressMiddleware(apollo, {
      context: async ({ req, res }) => ({ req, res, prisma }),
    })
  );
  app.listen(3000, () =>
    console.log(`🚀 Server ready at: http://localhost:3000`)
  );
};

main();
