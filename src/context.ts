import { PrismaClient } from '@prisma/client'
import express from 'express'

export type Context = {
  prisma: PrismaClient;
  req: express.Request;
  res: express.Response;
}