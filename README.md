# Hypercent Tx GQL

This is a GraphQL server for the Hypercent Tx explorer.

## Getting started

### Requirements:

docker, docker-compose, node, npm/yarn

```bash
# Install dependencies
yarn

# Start the server
export DATABASE_URL=postgres://user:password@localhost:5432/hypercent
yarn start
```

## Development

Update the prisma schema and run the following commands to update the database schema:

```bash
npx prisma migrate dev
```

Then you're ready to start the server.

## Deployment

That's up to you, but you can use the provided Dockerfile and docker-compose.yml to get started.

I prefer using ts-node as building would require me to generate the prisma client too, but that can be done.
Just do this in the Dockerfile:

```Dockerfile
RUN yarn
RUN npx prisma generate
RUN npx tsc

# Start the server

CMD node dist/index.js
```
