FROM node:20-alpine

COPY . /app

WORKDIR /app

RUN yarn

ENV DATABASE_URL="postgres://postgres:password@postgres:5432/hypercent"

CMD ["sh", "-c", "yarn prisma:migrate && yarn start"]
